/*
 * 引数のメッセージでポップアップを表示する。
 * ポップアップのキャンセルボタン押下時、falseを返却する。
 * ※formタグのonsubmit属性で使用すること。
 */
function submitCheck(message){
	if(!confirm(message)){
		return false;
	}
}