/**
 * 全画面共通で使用する関数
 */


/**
 * ステータスコード→ステータス名に変更する。
 * @param value
 * @returns
 */
Vue.filter('statusName', function (code) {

		switch(code){
			case 1: return '申請中';
			case 2: return '却下';
			case 3: return '貸出中';
			case 10: return '在庫あり';
			case 11: return '在庫なし';
			default: return '';
		}
	});


/**
 * URLのパラメータを取得する
 * @param name
 * @param url
 * @returns
 */
function getParam(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}