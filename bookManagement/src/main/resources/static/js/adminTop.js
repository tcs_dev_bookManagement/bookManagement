document.addEventListener("DOMContentLoaded", function(event) {

	// リポジトリクラス
	var rep = new repository();

	var app = new Vue({
		el : '#app',
		data : {
			/** 申請一覧 */
			bookRequestList : rep.getBookRequestList(),
			/** 書籍一覧 */
			bookList : [],
			/** 検索キーワード */
			keyword : '',
			/** 登録ダイアログの表示 */
			isDialog:false,
			/** 書籍 */
			book:{},
			/** 書籍ダイアログタイトル */
			dialogTitle: "",
			/** 状態 */
			statusList:['申請中','貸出中','却下']
		},
		created : function () {

			// 書籍の一覧を取得する。
			rep.getBookList()
			.then(response => {
				app.bookList = response.data;
			})
	    	.catch(error => console.log(error));

		},
		methods : {
			/**
			 * 書籍新規作成画面に遷移する。
			 */
			onOpenDialog: function(id){
				if(id === undefined){
					// 新規登録
					app.dialogTitle = "書籍の登録";
					app.book = {};
					app.book.allBookCount = 0;
				}else{
					// 変更・削除
					app.dialogTitle = "書籍内容の更新";
					app.book.name = "java本";
					app.book.writer = "海老沼";
					app.book.publishingYear = "2018-03-10";
					app.book.publishingHouse = "海老沼 慎一";
					app.book.allBookCount = 2;
					app.book.comment = "そんな対した本ではない！";
				}
				this.isDialog = true;
			},
			onDialogClose: function(){
				this.isDialog = false;
			},
			onCreateBook: function(){
				this.onDialogClose();
			},
			/**
			 * 検索条件をクリアする。
			 */
			onKeywordClear : function(event) {
				this.keyword='';
			},
			/**
			 * ログアウトを実施する。
			 */
			onLogout : function(event) {
				rep.logout();
			}

		},
	    computed: {
	    	/**
	    	 * 書籍一覧の検索条件フィルタ
	    	 */
	    	bookListFilter: function() {

	    		// 検索条件が無い場合は全件返す。
	    		if(this.keyword.trim() == ''){
	    			return this.bookList;
	    		}

	    		// 検索条件でフィルタリングする。
	    		var list = [];
	    		var keyword = this.keyword;
	    		this.bookList.forEach((book) => {
	    			 var bookName = book.name.toLowerCase();
	    			 var keywordName = keyword.trim().toLowerCase();
	    			 if(0 <= bookName.indexOf(keywordName)){
	    				 list.push(book);
	    			 }
	    		});
	            return list;
	        }
	    }
	});
});