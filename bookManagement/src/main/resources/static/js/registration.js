document.addEventListener("DOMContentLoaded", function(event) {

	// リポジトリクラス
	var rep = new repository();

	var app = new Vue({
		el : '#app',
		data : {
			/** 書籍情報 */
			book : createBook()
		},
		methods : {
			/**
			 * 書籍の新規作成を実施する。
			 */
			onCreate: function(event){
				alert("新規登録します。よろしいですか");
				rep.registBook(this.book);
			},
			/**
			 * 前画面に戻る
			 */
			onBack: function(event){
				document.location.href = "/book/adminTop.html";
			},
			/**
			 * ログアウトを実施する。
			 */
			onLogout : function(event) {
				rep.logout();
			}
		}
	});



	/**
	 * 空の書籍を返します。
	 */
	function createBook(){
		return  {
  		     bookName:'',
		     author:'',
		     publicationYear:'',
		     publicationCompany:'',
		     num:'',
		     comment:''
	    	}
	}
});
