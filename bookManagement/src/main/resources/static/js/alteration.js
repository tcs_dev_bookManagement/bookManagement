document.addEventListener("DOMContentLoaded", function(event) {

	// リポジトリクラス
	var rep = new repository();

	var app = new Vue({
		el : '#app',
		data : {
			/** 書籍情報 */
			book : {},
		},
		created : function () {
			// 書籍の詳細を取得する。
			rep.getBook(getParam("id"))
			.then(response => {
				app.book = response.data;
			})
	    	.catch(error => console.log(error));
		},
		methods : {
			/**
			 * 書籍の更新を実施する
			 */
			onUpdate: function(event){
				alert("書籍情報を変更します。よろしいですか。");
			},
			/**
			 * 書籍の削除を実施する
			 */
			onDelete: function(event){
				alert("書籍情報を削除します。よろしいですか。");
			},
			/**
			 * 前画面に戻る
			 */
			onBack: function(event){
				document.location.href = "/book/adminTop.html";
			},
			/**
			 * ログアウトを実施する。
			 */
			onLogout : function(event) {
				rep.logout();
			}
		}
	});
});



