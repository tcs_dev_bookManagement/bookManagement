document.addEventListener("DOMContentLoaded", function(event) {

	var app = new Vue({
		el : '#app',
		data : {
			/** アカウント */
			account : '',
			/** パスワード */
			password : ''
		},
		methods : {
			/**
			 * ログイン実施
			 */
			onLogin : function(event) {
				document.location.href = "/book/adminTop.html";
			}
		}
	});

});
