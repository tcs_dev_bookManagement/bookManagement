/**
 * 書籍システムの永続化情報にアクセスするクラス
 */
class repository {

    constructor() {
    	var protocol = location.protocol ;
    	var host = location.host;
    	this.baseUrl = protocol + "//" + host;
    }

    /**
	 * 申請済みの書籍の一覧を返します。
	 */
    getBookRequestList() {
    	var bookRequestList = [];
    	bookRequestList.push({bookName:'javaの本',author:'矢坂　仁',publicationYear:2018,publicationCompany:'集英社',user:'海老沼　新太郎',status:1,id:1});
    	bookRequestList.push({bookName:'cobolの本',author:'畑　コボル',publicationYear:2018,publicationCompany:'集英社',user:'海老沼　新太郎',status:2,id:2});
    	bookRequestList.push({bookName:'rubyの本',author:'神田 正人',publicationYear:2018,publicationCompany:'集英社',user:'海老沼　新太郎',status:3,id:3});
    	bookRequestList.push({bookName:'なんかの本',author:'神田 正人',publicationYear:2018,publicationCompany:'集英社',user:'海老沼　新太郎',status:3,id:4});
    	bookRequestList.push({bookName:'あれな本',author:'神田 正人',publicationYear:2018,publicationCompany:'集英社',user:'海老沼　新太郎',status:3,id:5});
    	bookRequestList.push({bookName:'こんな本',author:'神田 正人',publicationYear:2018,publicationCompany:'集英社',user:'海老沼　新太郎',status:3,id:6});
    	return bookRequestList;
    }


    /**
	 * 書籍の一覧を返します。
	 */
    getBookList(){
    	var token = "1111111111111111111111";
    	var config = {headers: {'Authorization': "token " + token}};
    	return axios .get(this.baseUrl+"/bookmanagement/api/book",config);
    }

    /**
	 * 指定のIDの書籍の詳細を返します。
	 */
    getBook(id){
    	var token = "1111111111111111111111";
    	var config = {headers: {'Authorization': "token " + token}};
    	return axios .get(this.baseUrl+"/bookmanagement/api/book/"+id,config);
    }


    /**
	 * 書籍の新規登録を行います。
	 */
    registBook(book){


    }

    /**
	 * 書籍の更新を行います。
	 */
    updateBook(id,book){

    }

    /**
	 * 書籍を削除します。
	 */
    deleteBook(id){

    }


    /**
	 * ログアウト処理を実施する。
	 */
    logout(){
    	// サーバー上のリソース削除
    	// ローカルストレージ削除
    	document.location.href = "/book/login.html";
    }
}