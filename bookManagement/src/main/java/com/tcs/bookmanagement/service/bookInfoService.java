package com.tcs.bookmanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tcs.bookmanagement.dao.bookInfoDao;
import com.tcs.bookmanagement.entity.BookInfomationEntity;


@Service
public class bookInfoService {
	@Autowired
	bookInfoDao bookDao;

	public List<BookInfomationEntity> selectAll() {
		return bookDao.findAll();
	}

	public BookInfomationEntity detail(Integer id) {
		return bookDao.detail(id);
	}

	public List<BookInfomationEntity> search(String keyward) {
		return bookDao.search(keyward);
	}

}
