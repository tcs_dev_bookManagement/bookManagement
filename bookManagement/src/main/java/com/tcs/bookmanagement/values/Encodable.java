package com.tcs.bookmanagement.values;

import java.io.Serializable;

/**
 *
 *
 * @param <T>
 */
public interface Encodable<T extends Serializable> {
	T encode();
}
