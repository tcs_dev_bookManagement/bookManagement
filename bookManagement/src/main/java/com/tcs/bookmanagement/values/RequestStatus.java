package com.tcs.bookmanagement.values;

import java.util.ArrayList;
import java.util.List;

import com.tcs.bookmanagement.entity.RequestStatusElement;

/**
 * 申請状態
 *
 */
public enum RequestStatus implements Encodable<Integer>{
	REQUESTING(1, "申請中"),
	CANCEL(2, "申請取消"),
	REJECTION(3, "申請却下"),
	CLOSE(4, "却下完了"),
	APPROVAL(5, "承認済"),
	BRINGING(6, "持出中"),
	RETURNING(7, "返却中"),
	FINISH(8, "返却完了");

	/** コード */
	private final Integer code;
	/** 表示名 */
	private final String label;
	/** デコーダー */
	private static final Decoder<Integer, RequestStatus> decoder = Decoder.getInstance(values());

	/**
	 * コンストラクタ
	 *
	 * @param code コード
	 * @param label 表示名
	 */
	private RequestStatus(Integer code, String label) {
		this.code = code;
		this.label = label;
	}

	/**
	 * デコード
	 * @param code
	 * @return enum
	 */
	public static RequestStatus decode(Integer code) {
        return decoder.decode(code);
    }

	/**
	 * エンコード
	 * @return enum
	 */
	@Override
	public Integer encode() {
		return code;
	}

	/**
	 * 表示名取得
	 *
	 * @return 表示名
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * 申請状態要素リスト取得
	 * @param status
	 * @return
	 */
	public static List<RequestStatusElement> getRequestStatusElements(Integer code){
		List<RequestStatusElement> RequestStatusElements = new ArrayList<>();

		RequestStatus status = decode(code);

		switch(status){
		case REQUESTING:
			/*
			 * 申請中の場合
			 * ・申請中
			 * ・申請取消
			 */
			RequestStatusElements.add(new RequestStatusElement(REQUESTING.encode(), REQUESTING.getLabel()));
			RequestStatusElements.add(new RequestStatusElement(CANCEL.encode(), CANCEL.getLabel()));
			break;

		case REJECTION:
			/*
			 * 申請却下の場合
			 * ・申請中
			 * ・申請却下
			 * ・却下完了
			 */
			RequestStatusElements.add(new RequestStatusElement(REQUESTING.encode(), REQUESTING.getLabel()));
			RequestStatusElements.add(new RequestStatusElement(REJECTION.encode(), REJECTION.getLabel()));
			RequestStatusElements.add(new RequestStatusElement(CLOSE.encode(), CLOSE.getLabel()));
			break;

		case APPROVAL:
			/*
			 * 承認済の場合
			 *・承認済
			 *・持出中
			 */
			RequestStatusElements.add(new RequestStatusElement(APPROVAL.encode(), APPROVAL.getLabel()));
			RequestStatusElements.add(new RequestStatusElement(BRINGING.encode(), BRINGING.getLabel()));
			break;

		case BRINGING:
			/*
			 * 持出中の場合
			 * ・承認済
			 * ・持出中
			 * ・返却中
			 */
			RequestStatusElements.add(new RequestStatusElement(APPROVAL.encode(), APPROVAL.getLabel()));
			RequestStatusElements.add(new RequestStatusElement(BRINGING.encode(), BRINGING.getLabel()));
			RequestStatusElements.add(new RequestStatusElement(RETURNING.encode(), RETURNING.getLabel()));
			break;

		case RETURNING:
			/*
			 * 返却中の場合
			 * ・返却中
			 * ・返却完了
			 */
			RequestStatusElements.add(new RequestStatusElement(RETURNING.encode(), RETURNING.getLabel()));
			RequestStatusElements.add(new RequestStatusElement(FINISH.encode(), FINISH.getLabel()));
			break;

		default:
			/*
			 * 下記のステータスの場合、非表示のため設定なし
			 * ・申請取消
			 * ・却下完了
			 * ・返却完了
			 */
		}

		return RequestStatusElements;
	}
}
