package com.tcs.bookmanagement.values;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Decoder<K extends Serializable, V extends Encodable<K>> {
	/** enum逆引き用マップ */
	private Map<K, V> map;

	/**
	 * コンストラクタ<br>
	 * enum逆引き用マップを生成する。<br>
	 *
	 * @param values
	 */
	private Decoder(V[] values) {
		map = new HashMap<K, V>(values.length);
		for(V value : values) {
			/* コード値の重複はサポートしない */
			if(map.put(value.encode(), value) != null) {
				throw new IllegalArgumentException("重複コード：" + value);
			}
		}
	}

	/**
	 * デコード<br>
	 * コード値に対応するenumを返却する。<br>
	 *
	 * @param code
	 * @return コード値
	 */
	public V decode(K code) {
		return map.get(code);
	}

	/**
	 * インスタンス生成<br>
	 * プライベートコンストラクタを呼び出し、インスタンスを返却する。<br>
	 *
	 * @param values
	 * @return インスタンス
	 */
	public static <K1 extends Serializable, V1 extends Encodable<K1>> Decoder<K1, V1>
	getInstance(V1[] values) {
        return new Decoder<K1, V1>(values);
    }

}
