package com.tcs.bookmanagement.api;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.bookmanagement.dummy.DummyUtil;
import com.tcs.bookmanagement.entity.LendEntity;
import com.tcs.bookmanagement.values.RequestStatus;

@RestController
@RequestMapping("/bookmanagement/api/lend")
public class LendApi {
	/* TODO 全体部会用暫定対処 DBに接続する。今はダミー値を返却 */

	/**
	 * 申請ステータス一覧取得<br>
	 * 社員IDに紐づく申請ステータス一覧を取得する<br>
	 *
	 * @param staffId 社員ID
	 * @return 申請ステータス一覧
	 */
	@RequestMapping(method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<LendEntity> getStatusList(
			@RequestParam(name="staff_id", defaultValue = "") String staffId){

		List<LendEntity> result = DummyUtil.createDummyStatusList();
		for(LendEntity lendEntity : result) {
			lendEntity.setRequestStatusElements(
					RequestStatus.getRequestStatusElements(lendEntity.getRequestStatus()));
		}
		return result;
	}

}
