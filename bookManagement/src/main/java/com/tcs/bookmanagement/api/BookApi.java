package com.tcs.bookmanagement.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.bookmanagement.entity.BookInfomationEntity;
import com.tcs.bookmanagement.service.bookInfoService;

@RestController
@RequestMapping(value = "/bookmanagement/api/book")
public class BookApi {

	@Autowired
	public bookInfoService service;

	/**
	 * 書籍一覧取得<br>
	 * 検索キーワードの指定がある場合はキーワードを元に検索、指定がない場合は全件検索する<br>
	 *
	 * @param keyword 検索キーワード(未設定時は空文字)
	 * @return 書籍一覧
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<BookInfomationEntity> geBookList(
			@RequestParam(name="keyword", defaultValue = "") String keyword) {
		return keyword == "" ? service.selectAll() : service.search(keyword);
	}

	/**
	 * 詳細情報取得<br>
	 * idに紐づく書籍の詳細情報を返却する<br>
	 *
	 * @param id
	 * @return 詳細情報
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value="{id}")
	@ResponseBody
	public BookInfomationEntity toSearch(@PathVariable Integer id) {
		return service.detail(id);
	}


}
