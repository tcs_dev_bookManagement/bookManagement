package com.tcs.bookmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookManagementViewApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookManagementViewApplication.class, args);
	}
}
