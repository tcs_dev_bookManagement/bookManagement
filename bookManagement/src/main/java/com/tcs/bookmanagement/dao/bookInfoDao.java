package com.tcs.bookmanagement.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.tcs.bookmanagement.entity.BookInfomationEntity;

/**
 * 本TBLと接続してる。
 *
 *
 */
public interface bookInfoDao extends JpaRepository<BookInfomationEntity, Integer> {

	@Query("from BookInfomationEntity where name Like %?1% ")
	public List<BookInfomationEntity> search(String keyward);

	@Query("from BookInfomationEntity where id = ?1 ")
	public BookInfomationEntity detail(Integer id);

}
