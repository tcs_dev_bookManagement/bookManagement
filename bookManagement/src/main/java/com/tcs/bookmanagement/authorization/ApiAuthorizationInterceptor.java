/**
 *
 */
package com.tcs.bookmanagement.authorization;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

/**
 * 認証実施クラス
 * @author user
 * @see https://qiita.com/dmnlk/items/cce551ce18973f013b36
 *
 */
public class ApiAuthorizationInterceptor implements HandlerInterceptor {

//	@Autowired
//	private AuthorizationService authService;

	/**
	 * コントラーが呼ばれる前に実行するメソッド
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		//静的リソースの場合は認証不要
		if (handler instanceof ResourceHttpRequestHandler) {
			return true;
		}

		// @NonAuthがついてるメソッドは認証不要
		if (isNonAuth(handler)) {
			return true;
		}

		// 認証を実施する
		//auth(request);

		return true;
	}

	/**
	 * メソッドにNonAuthのアノテーションが付与されているか確認する。
	 * @param handler
	 * @return
	 */
	private boolean isNonAuth(Object handler) {
		if (handler instanceof HandlerMethod) {
			HandlerMethod hm = (HandlerMethod) handler;
			Method method = hm.getMethod();
			NonAuth annotation = AnnotationUtils.findAnnotation(method, NonAuth.class);
			if (annotation != null) {
				return true;
			}else {
				return false;
			}
		}
		return true;
	}

	/**
	 * 認証を実施する
	 * @param request
	 * @throws UnauthorixedException
	 */
//	private void auth(HttpServletRequest request) throws UnauthorixedException {
//		String token = request.getHeader("Authorization");
//		if (StringUtils.isEmpty(token)) {
//			throw new UnauthorixedException("認証されていません");
//		}
//
//		authService.isAuth(token.replaceFirst("token", "").trim());
//	}

}
