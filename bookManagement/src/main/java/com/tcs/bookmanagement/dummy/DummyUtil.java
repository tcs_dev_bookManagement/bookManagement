package com.tcs.bookmanagement.dummy;

import java.util.ArrayList;
import java.util.List;

import com.tcs.bookmanagement.entity.LendEntity;

/**
 * DBから取得する部分のダミーデータを作る
 *
 */
public class DummyUtil {

	/**
	 * 申請状況一覧を作成
	 */
	public static List<LendEntity> createDummyStatusList(){
		List<LendEntity> statusList = new ArrayList<>();
		/* 1件目 */
		LendEntity se1 = new LendEntity();
		se1.setLendId(123);
		se1.setName("基本情報技術者試験の教科書");
		se1.setWriter("稲田　史郎");
		se1.setPublishingYear("2018");
		se1.setPublishingHouse("ぺふ社");
		se1.setRequestStatus(1);
		statusList.add(se1);
		/* 2件目 */
		LendEntity se2 = new LendEntity();
		se2.setLendId(456);
		se2.setName("JavaBronzeの教科書");
		se2.setWriter("永山　松子");
		se2.setPublishingYear("2017");
		se2.setPublishingHouse("ぬるぽ社");
		se2.setRequestStatus(5);
		statusList.add(se2);
		return statusList;
	}
}
