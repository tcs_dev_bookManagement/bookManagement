package com.tcs.bookmanagement.entity;

import java.util.List;

/**
 * 貸出ＴＢＬに紐づく
 *
 */
public class LendEntity {
	/* TODO 全体部会用暫定対処 DBに接続する。今はダミー値を返却 */
	/* TODO 全体部会用暫定対処 申請状態のコードの持ち方要検討 */

	/** 貸出ID */
	private Integer lendId;
	/** 社員ID */
	private Integer staffId;
	/** 書籍ID */
	private Integer bookId;
	/** タイトル */
    private String name;
	/** 著者 */
	private String writer;
	/** 出版年 */
	private String publishingYear;
	/** 出版社 */
	private String publishingHouse;
	/** 申請状態 */
	private Integer requestStatus;
	/** 申請状態要素リスト */
	List<RequestStatusElement> RequestStatusElements;

	/**
	 * @return lendId
	 */
	public Integer getLendId() {
		return lendId;
	}
	/**
	 * @param lendId セットする lendId
	 */
	public void setLendId(Integer lendId) {
		this.lendId = lendId;
	}
	/**
	 * @return staffId
	 */
	public Integer getStaffId() {
		return staffId;
	}
	/**
	 * @param staffId セットする staffId
	 */
	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}
	/**
	 * @return bookId
	 */
	public Integer getBookId() {
		return bookId;
	}
	/**
	 * @param bookId セットする bookId
	 */
	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}
	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name セットする name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return writer
	 */
	public String getWriter() {
		return writer;
	}
	/**
	 * @param writer セットする writer
	 */
	public void setWriter(String writer) {
		this.writer = writer;
	}
	/**
	 * @return publishingYear
	 */
	public String getPublishingYear() {
		return publishingYear;
	}
	/**
	 * @param publishingYear セットする publishingYear
	 */
	public void setPublishingYear(String publishingYear) {
		this.publishingYear = publishingYear;
	}
	/**
	 * @return publishingHouse
	 */
	public String getPublishingHouse() {
		return publishingHouse;
	}
	/**
	 * @param publishingHouse セットする publishingHouse
	 */
	public void setPublishingHouse(String publishingHouse) {
		this.publishingHouse = publishingHouse;
	}
	/**
	 * @return requestStatus
	 */
	public Integer getRequestStatus() {
		return requestStatus;
	}
	/**
	 * @param requestStatus セットする requestStatus
	 */
	public void setRequestStatus(Integer requestStatus) {
		this.requestStatus = requestStatus;
	}
	/**
	 * @return requestStatusElements
	 */
	public List<RequestStatusElement> getRequestStatusElements() {
		return RequestStatusElements;
	}
	/**
	 * @param requestStatusElements セットする requestStatusElements
	 */
	public void setRequestStatusElements(List<RequestStatusElement> requestStatusElements) {
		RequestStatusElements = requestStatusElements;
	}
}
