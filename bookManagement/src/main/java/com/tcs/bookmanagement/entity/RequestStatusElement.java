package com.tcs.bookmanagement.entity;

/**
 * 申請状態要素<br>
 * 申請状態プルダウンを作成するための情報をもつ。<br>
 *
 */
public class RequestStatusElement {

	/** コード */
	private Integer code;
	/** 表示名 */
	private String label;

	/**
	 * コンストラクタ
	 */
	public RequestStatusElement(Integer code, String label) {
		this.code = code;
		this.label = label;
	}

	/**
	 * @return code
	 */
	public Integer getCode() {
		return code;
	}
	/**
	 * @param code セットする code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	/**
	 * @return label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label セットする label
	 */
	public void setLabel(String label) {
		this.label = label;
	}



}
