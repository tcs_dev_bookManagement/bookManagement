package com.tcs.bookmanagement.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * DBから取得した書籍情報をもつ
 *
 */
@Entity
@Table(name="book")
public class BookInfomationEntity implements Serializable {

	/** ID */
	@Id
    @Column(name="book_id")
    private Integer id;
	/** タイトル */
    @Column(name="book_name")
    private String name;
	/** 著者 */
    @Column(name="author")
	private String writer;
	/** 出版年 */
    @Column(name="publication_year")
	private String publishingYear;
	/** 出版社 */
    @Column(name="publication_company")
	private String publishingHouse;
	/** コメント */
	@Column(name="comment")
	private String comment;
	/** 総冊数 */
	@Column(name="all_book_count")
	private int allBookCount;
	/** 貸出冊数 */
	@Column(name="bring_book_count")
	private int bringBookCount;
	/** 残冊数 */
	@Transient
	private int remainingCount;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	public String getPublishingYear() {
		return publishingYear;
	}
	public void setPublishingYear(String publishingYear) {
		this.publishingYear = publishingYear;
	}
	public String getPublishingHouse() {
		/* TODO 全体部会用暫定対処 （あきらめ）
		 * 年だけ欲しいがゲッターに処理を書いても実行されない模様。SQLの方でどうにかする？
		 * とりあえず画面側でsubstringする */
		substringPublishingYear();
		return publishingHouse;
	}
	public void setPublishingHouse(String publishingHouse) {
		this.publishingHouse = publishingHouse;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public int getAllBookCount() {
		return allBookCount;
	}
	public void setAllBookCount(int allBookCount) {
		this.allBookCount = allBookCount;
	}
	public int getLendBookCount() {
		return bringBookCount;
	}
	public void setLendBookNumber(int bringBookCount) {
		this.bringBookCount = bringBookCount;
	}

	public void substringPublishingYear() {
		if(!this.publishingYear.isEmpty() && this.publishingYear.length() > 4 ) {
			this.publishingYear = this.publishingYear.substring(0,4) + "年";
		}
	}

	public int getRemainingCount() {
		/* TODO 全体部会用暫定対処 とりあえずget時に残冊数を計算して返す。セッターはたぶん使われない */
		this.remainingCount = allBookCount - bringBookCount;
		return remainingCount;
	}

	public void setRemainingCount() {
		this.remainingCount = allBookCount - bringBookCount;
	}


}
